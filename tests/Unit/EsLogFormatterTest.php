<?php

namespace Naresh\ElasticSearchLogger\Tests\Unit;

use Naresh\ElasticSearchLogger\EsLogFormatter;
use Naresh\ElasticSearchLogger\Tests\TestCase;

/**
 * Class EsLogFormatterTest
 * @package Naresh\ElasticSearchLogger\Tests\Unit
 */
class EsLogFormatterTest extends TestCase
{


    public function testFormat()
    {
        $expected = ["this is test log"];
        $obj = new EsLogFormatter();
        $actual = $obj->format($expected);
        $this->assertEquals($expected, $actual);
    }
}