<?php

namespace Naresh\ElasticSearchLogger;

use Monolog\Formatter\NormalizerFormatter;

/**
 * Class EsLogFormatter
 * @package Naresh\ElasticSearchLogger\Services
 */
class EsLogFormatter extends NormalizerFormatter
{

    /**
     */
    public function __construct()
    {
        /**
         * elasticsearch requires a ISO 8601 format date with optional millisecond precision.
         */
        parent::__construct('Y-m-d\TH:i:s.uP');
    }

    /**
     * {@inheritdoc}
     */
    public function format(array $record)
    {
        $record = parent::format($record);
        return $record;
    }
}