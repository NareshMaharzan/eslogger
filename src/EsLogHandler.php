<?php

namespace Naresh\ElasticSearchLogger;

use Monolog\Handler\AbstractProcessingHandler;
use Monolog\Formatter\FormatterInterface;
use Elasticsearch\Client;
use Monolog\Logger;
use Log;

/**
 * Class EsLogHandler
 * @package Naresh\ElasticSearchLogger\Services
 */
class EsLogHandler extends AbstractProcessingHandler
{

    /**
     * @var Client
     */
    protected $client;

    /**
     * @var array Handler config options
     */
    protected $options = [];

    /**
     * @param Client $client Elasticsearch\Client object
     * @param array $options
     * @param int $level The minimum logging level at which this handler will be triggered
     * @param Boolean $bubble Whether the messages that are handled can bubble up the stack or not
     */
    public function __construct($client, array $options, $level = Logger::DEBUG, $bubble = true)
    {
        parent::__construct($level, $bubble);
        $this->client = $client;
        $this->options = $options;
    }

    /**
     * {@inheritDoc}
     */
    protected function write(array $record)
    {
        $this->index($record['formatted']);
    }

    /**
     * {@inheritdoc}
     */
    public function setFormatter(FormatterInterface $formatter)
    {
        if ($formatter instanceof EsLogFormatter) {
            return parent::setFormatter($formatter);
        }
        throw new \InvalidArgumentException('EsLogHandler is only compatible with EsLogFormatter');
    }

    /**
     * Getter options
     * @return array
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * {@inheritDoc}
     */
    protected function getDefaultFormatter()
    {
        return new EsLogFormatter();
    }
    
    /**
     * Use Elasticsearch bulk API to send list of documents
     * @param  array $document
     */
    protected function index(array $document)
    {
        $params = [
            'index' => $this->options['index'],
            'type' => $this->options['index_type'],
            'body' => $document
        ];
        try {
            $this->client->index($params);
        } catch (\Exception $e) {
            Log::channel('errorlog')->error($e->getMessage());
        }
    }
}