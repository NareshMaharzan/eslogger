<?php

namespace Naresh\ElasticSearchLogger\Tests\Unit;

use Naresh\ElasticSearchClient\ElasticsearchClient;
use Naresh\ElasticSearchLogger\EsLogHandler;
use Naresh\ElasticSearchLogger\EsLogService;
use Naresh\ElasticSearchLogger\Tests\TestCase;
use Elasticsearch\Client;
use Monolog\Logger;


/**
 * Class EsLogServiceTest
 * @package Naresh\ElasticSearchLogger\Tests\Unit
 */
class EsLogServiceTest extends TestCase
{

    public function testGetEsClientInstance()
    {
        $obj = new EsLogService();
        $actual = $obj->getEsClientInstance();
        $this->assertInstanceOf(ElasticsearchClient::class, $actual);
    }

    public function testGetEsLogHandlerInstance()
    {
        $client = \Mockery::mock(Client::class)->makePartial();
        $obj = new EsLogService();
        $actual = $obj->getEsLogHandlerInstance($client, [], 'info');
        $this->assertInstanceOf(EsLogHandler::class, $actual);
    }

    public function testGetLoggerInstance()
    {
        $obj = new EsLogService();
        $actual = $obj->getLoggerInstance();
        $this->assertInstanceOf(Logger::class, $actual);
    }


    public function testGetOptions()
    {
        $obj = new EsLogService();
        $actual = $obj->getOptions();
        $this->assertEquals([
                "hosts" => ["test.es-server.com"],
                "aws_host" => ""
                ,
                "access_key" => "LK9823kjhsd"
                ,
                "secret_key" => "LK9823kjhsd"
                ,
                "region" => "ap-southeast-2"
                ,
                "index" => "testIndex"
                ,
                "index_type" => "testIndexType"
            ]
            , $actual);
    }


}