<?php
namespace Naresh\ElasticSearchLogger\Tests\Unit;


use Naresh\ElasticSearchClient\ElasticsearchClient;
use Naresh\ElasticSearchLogger\EsLog;
use Naresh\ElasticSearchLogger\EsLogFacade;
use Naresh\ElasticSearchLogger\EsLogHandler;
use Naresh\ElasticSearchLogger\Tests\TestCase;
use Monolog\Logger;
use Illuminate\Support\Facades\App;


/**
 * Class EsLogTest
 * @package Naresh\ElasticSearchLogger\Tests\Unit
 */
class EsLogTest extends TestCase
{

    public function testInvoke()
    {
        App::shouldReceive('environment')->andReturn('testing');

        $obj = new EsLog();
        $actual = $obj(['options' => [
            'hosts' => ['test.abc.com'],
            'aws_host' => false,
            'index' => 'ES_LOG_INDEX',
            'index_type' => 'ES_LOG_INDEX_TYPE',
        ]]);
        $this->assertInstanceOf(Logger::class, $actual);
    }
}