# ElasticSearch logging library for laravel 5.6

#### Note: You can use Molong Elasticsearch Handler to achieve same behaviour as this library. However it only supports [Elastica](http://elastica.io/) client. 
 
If you want to use Elasticsearch logging channel for logging(Laravel >=5.6) then this library is for you.

## Installation

Require package in composer.json file.
```json
    "require":{
        "naresh/eslogger": "1.0.*"
    }

```
And run composer update to update the dependency.

## Configuration

* Add __Elasticsearch logger__ in your `config/logging.php` 
```php
'channels' => [
    'custom' => [
        'driver' => 'custom',
        'via' => \Naresh\ElasticSearchLogger\EsLog::class,
    ],
],
```

* Add __Elasticsearch configuration__
    * ___Option 1___ Configure with __ENV__ variables
        * ES_HOST
            * Elasticsearch host
        
        * IS_AWS_ES_HOST
            * Boolean flag to specify if the hostname is ___AWS Elasticsearch service___, however if your host has `amazonaws` in its URL then you dont have to add this flag, this library will assume it as ___AWS Elasticsearch service___
        
        * AWS_ACCESS_KEY_ID(only needed if host is ___AWS Elasticsearch service___)
            * ___AWS Elasticsearch service___ access key 
        
        * AWS_SECRET_ACCESS_KEY(only needed if host is ___AWS Elasticsearch service___)
            * ___AWS Elasticsearch service___ secret key 
        
        * AWS_REGION(only needed if host is ___AWS Elasticsearch service___)
            * ___AWS Elasticsearch service___ region
        
        * ES_LOG_INDEX
            * Elasticsearch Index where you want to send the logs
        
        * ES_LOG_INDEX_TYPE
             * Elasticsearch Index type
     
     * __Option 2__ If you do not want to use __ENV__ variables then you can configure it while setting it in the `config/logging.php` file.
     
        * By creating new instance
            * Create a new instance of `\Naresh\ElasticSearchLogger\EsLog` like following 
                * `$eslogger = new \Naresh\ElasticSearchLogger\EsLog();`
            * Then pass the configurations to the `EsLog` instance, 
                * ```php
                  'channels' => [
                      'custom' => [
                          'driver' => 'custom',
                          'via' => $esLogger(['options' => ['ES_HOST' => 'test.es.com']]),
                      ],
                  ],
                  ```
        * By passing options in logging configuration
            * ```php
                'channels' => [
                    'custom' => [
                        'driver' => 'custom',
                        'via' => \Naresh\ElasticSearchLogger\EsLog::class,
                        'options' => [
                             "hosts" => ["test.es-server.com"],
                             "aws_host" => "",
                             "access_key" => "LK9823kjhsd",
                             "secret_key" => "LK9823kjhsd",
                             "region" => "ap-southeast-2",
                             "index" => "testIndex",
                             "index_type" => "testIndexType"]
                        ],
                ],
                ```