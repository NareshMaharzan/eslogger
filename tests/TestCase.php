<?php
namespace Naresh\ElasticSearchLogger\Tests;

use Naresh\ElasticSearchLogger\EsLogFacade;
use Naresh\ElasticSearchLogger\ServiceProvider;
use Orchestra\Testbench\TestCase as OrchestraTestCase;

/**
 * Class TestCase
 * @package Naresh\ElasticSearchLogger\Tests
 */
class TestCase extends OrchestraTestCase
{

    /**
     * Load package service provider
     * @param  \Illuminate\Foundation\Application $app
     * @return array
     */
    protected function getPackageProviders(/** @scrutinizer ignore-unused */ $app)
    {
        return [ServiceProvider::class];
    }
    /**
     * Load package alias
     * @param  \Illuminate\Foundation\Application $app
     * @return array
     */
    protected function getPackageAliases(/** @scrutinizer ignore-unused */ $app)
    {
        return [
            'EsClient' => EsLogFacade::class,
        ];
    }
}