<?php

namespace Naresh\ElasticSearchLogger;

use Illuminate\Support\Facades\App;
use Monolog\Logger;


/**
 * Class EsLog
 * @package Naresh\ElasticSearchLogger\Services
 */
class EsLog
{
    /**
     * Create a custom Monolog instance.
     *
     * @return Logger
     */
    public function __invoke(array $options = [])
    {
        $logLevel = $options['level'] ?? 'error';
        $esClientObj = EsLogFacade::getEsClientInstance();
        $options = !empty($options['options']) ? $options['options'] : EsLogFacade::getOptions();
        $client = $esClientObj->getClient($options);
        $handler = EsLogFacade::getEsLogHandlerInstance($client, $options, $logLevel);
        $channel = App::environment() ?? 'local';
        $log = EsLogFacade::getLoggerInstance($channel);
        $log->pushHandler($handler);
        return $log;
    }
}