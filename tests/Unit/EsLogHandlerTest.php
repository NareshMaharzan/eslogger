<?php

namespace Naresh\ElasticSearchLogger\Tests\Unit;


use Naresh\ElasticSearchLogger\EsLogFormatter;
use Naresh\ElasticSearchLogger\EsLogHandler;
use Naresh\ElasticSearchLogger\Tests\TestCase;
use Elasticsearch\Client;
use Illuminate\Support\Facades\Log;
use Monolog\Formatter\ElasticaFormatter;
use Monolog\Logger;

/**
 * Class EsLogHandlerTest
 * @package Naresh\ElasticSearchLogger\Tests\Unit
 */
class EsLogHandlerTest extends TestCase
{

    public function testSetFormatter()
    {
        $client = \Mockery::mock(Client::class)->makePartial();
        $formatter = new EsLogFormatter();
        $obj = new EsLogHandler($client, []);
        $actual = $obj->setFormatter($formatter);
        $this->assertInstanceOf(EsLogHandler::class, $actual);
    }

    public function testSetFormatterWithException()
    {
        $this->expectException(\InvalidArgumentException::class);
        $client = \Mockery::mock(Client::class)->makePartial();
        $formatter = new ElasticaFormatter("test", "test");
        $obj = new EsLogHandler($client, []);
        $obj->setFormatter($formatter);
    }

    public function testGetOptions()
    {
        $client = \Mockery::mock(Client::class)->makePartial();
        $obj = new EsLogHandler($client, []);
        $actual = $obj->getOptions();
        $this->assertEquals([], $actual);
    }

    public function testProtectedMethods()
    {
        $client = \Mockery::mock(Client::class)->makePartial();
        $client->shouldReceive('index');
        $obj = new EsLogHandler($client, [
            'index' => 'index',
            'index_type' => 'index_type',
        ]);
        $logger = with(new Logger('test-logger'))->pushHandler($obj);
        $logger->addInfo("this is test message");
    }

    public function testProtectedMethodsWithException()
    {
        Log::shouldReceive('channel')->andReturnSelf()->shouldReceive('error');
        $client = \Mockery::mock(Client::class)->makePartial();
        $client->shouldReceive('index')->andThrow(\Exception::class);
        $obj = new EsLogHandler($client, [
            'index' => 'index',
            'index_type' => 'index_type',
        ]);
        $logger = with(new Logger('test-logger'))->pushHandler($obj);
        $logger->addInfo("this is test message");
    }
}