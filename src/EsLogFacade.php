<?php
namespace Naresh\ElasticSearchLogger;


use Elasticsearch\Client;
use Illuminate\Support\Facades\Facade;

/**
 * Class EsLogFacade
 * @package Naresh\ElasticSearchLogger
 *
 * @method static getEsClientInstance(): ElasticsearchClient
 * @uses EsLogService::getEsClientInstance()
 *
 * @method static getEsLogHandlerInstance(Client $client, array $options, string $logLevel): EsLogHandler
 * @uses EsLogService::getEsLogHandlerInstance()
 *
 * @method static getLoggerInstance(string $name): Logger
 * @uses EsLogService::getLoggerInstance()
 *
 * @method static getOptions(): array
 * @uses EsLogService::getOptions()
 *
 */
class EsLogFacade extends Facade
{
    /**
     * Get the registered name of the Token.
     *
     * @return string
     */
    protected static function getFacadeAccessor(): string
    {
        return 'EsLogFacade';
    }
}