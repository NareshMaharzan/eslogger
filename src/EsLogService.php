<?php

namespace Naresh\ElasticSearchLogger;

use Naresh\ElasticSearchClient\ElasticsearchClient;
use Elasticsearch\Client;
use Monolog\Logger;

/**
 * Class EsLogService
 * @package Naresh\ElasticSearchLogger
 */
class EsLogService
{

    /**
     * Get ElasticsearchClient instance
     * @return ElasticsearchClient
     */
    public function getEsClientInstance()
    {
        return new ElasticsearchClient();
    }

    /**
     * Get EsLogHandler instance
     * @param Client $client
     * @param array $options
     * @param string $logLevel
     * @return EsLogHandler
     */
    public function getEsLogHandlerInstance(Client $client, array $options, string $logLevel)
    {
        return new EsLogHandler($client, $options, $this->getLogLevel($logLevel));
    }

    /**
     * Get Logger instance
     * @param string $name
     * @return Logger
     */
    public function getLoggerInstance(string $name = 'Elasticsearch Logger')
    {
        return new Logger($name);
    }

    /**
     * Get options
     * @return array
     */
    public function getOptions()
    {
        return [
            'hosts' => array_map('trim', explode(',', env('ES_HOST'))),
            'aws_host' => env('IS_AWS_ES_HOST') ?? false,
            'access_key' => env('AWS_ACCESS_KEY_ID'),
            'secret_key' => env('AWS_SECRET_ACCESS_KEY'),
            'region' => env('AWS_REGION'),
            'index' => env('ES_LOG_INDEX'),
            'index_type' => env('ES_LOG_INDEX_TYPE'),
        ];
    }

    /**
     * Get Monolog log level
     * @param $logLevel
     * @return mixed
     */
    public function getLogLevel($logLevel)
    {
        $levels = [
            'debug' => Logger::DEBUG,
            'info' => Logger::INFO,
            'notice' => Logger::NOTICE,
            'warning' => Logger::WARNING,
            'error' => Logger::ERROR,
            'critical' => Logger::CRITICAL,
            'alert' => Logger::ALERT,
            'emergency' => Logger::EMERGENCY,
        ];
        return $levels[$logLevel];
    }
}