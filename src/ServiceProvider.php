<?php
declare(strict_types=1);

namespace Naresh\ElasticSearchLogger;

use Illuminate\Support\ServiceProvider as Provider;

/**
 * ElasticSearchLogger ServiceProvider
 * @package Naresh\ElasticSearchLogger
 */
class ServiceProvider extends Provider
{
    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('EsLogFacade', EsLogService::class);
    }
}
